const uuid = require('uuid/v4')
const session = require('express-session')
const FileStore = require('session-file-store')(session);
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const port = process.env.PORT || 3000;

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(session({
  genid: (req) => {
    return uuid() 
  },
  store: new FileStore(),
  secret: 'insuranceapi',
  resave: false,
  saveUninitialized: true,
  expires : 60000,
}))

require('./app/routes')(app);

app.listen(port, () => {
  console.log('Listening on localhost:'+ port)
})

module.exports = app; 
