
const axios = require('axios');

module.exports = {

    auth: clientId => {
        return axios.get(`http://localhost:3000/v1/clients/${clientId}`)
    },

    requireRole: role => {
        return function (req, res, next) {
            if (req.session && (req.session.user && req.session.user.role === role)) {
                next();
            } else {
                res.status(403).send({ message: "Forbidden" });
            }
        }
    }

}