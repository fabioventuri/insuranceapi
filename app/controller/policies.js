
const _ = require('lodash')
const policiesService = require('../services/policies')
const clientsService = require('../services/clients')

module.exports = {

    getPoliciesByClientName: (req, res) => {
        let clientName = req.params.name;
        if (!clientName) return res.status(400).send({ "message": "" })

        clientsService.getClientsByName(clientName).then(clients => {

            if (!clients || (clients && clients.lentgh == 0)) 
                return res.status(404).send({ "message": "Client not found" })

            policiesService.getPoliciesByClientId(clients[0].id).then(result => {
                return res.status(200).send(result)
            }).catch(err => { return res.status(500).send(err) })

        }).catch(err => { return res.status(500).send(err) })
    },

}

