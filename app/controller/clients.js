
const _ = require('lodash')
const clientsService = require('../services/clients')
const policiesService = require('../services/policies')

module.exports = {

    getClient: (req, res) => {
        let clientID = req.params.id;
        clientsService.getClient(clientID)
        .then(result => {
            if (!result) res.status(404).send({ "message": "user not found" });
            return res.send(result);
        }).catch(err => res.status(500).send(err))
    },

    getClients: (req, res) => {
        clientsService.getClients()
        .then(result => res.status(200).send(result.data))
        .catch(err => res.status(500).send(err))
    },

    getClientByName: (req, res) => {
        let name = req.params.name;
        clientsService.getClientsByName(name)
        .then(result => {
            return res.send(result);
        }).catch(err => res.status(500).send(err))
    },

    getClientByPolicy: (req, res) => {
        let policyId = req.params.id;
        policiesService.getPolicy(policyId)
        .then(policy => {
            if (!policy) return res.status(404);
            clientsService.getClient(policy.clientId)
                .then(client => res.status(200).send(client))
                .catch(err => res.status(500).send(err))
        })
    },
}
