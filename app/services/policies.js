const mockyRepository = require('../repository/mocky')
const _ = require('lodash')

module.exports = {

    getPolicy: (id) => {
        return new Promise((resolve, reject) => {
            mockyRepository.getPolicies().then(result => {
                let policies = result.data.policies
                let policy = _.find(policies, { 'id': id })
                return resolve(policy)
            }).catch(err => reject(err))
        })
    },
    getPoliciesByClientId: (clientId) => {
        return new Promise((resolve, reject) => {
            mockyRepository.getPolicies().then(result => {
                let policies = result.data.policies;
                let clientPolicies = _.filter(policies, (policy) => {
                    return policy.clientId == clientId;
                });
                return resolve(clientPolicies)
            }).catch(error => reject(error))
        })
    }

}

