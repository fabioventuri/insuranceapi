
const mockyRepository = require('../repository/mocky')
const _ = require('lodash')

module.exports = {
    
    getClient: id => {
        return new Promise((resolve, reject) => {
            mockyRepository.getClients().then(result => {
                let client = _.find(result.data.clients, { 'id': id });
                return resolve(client);
            }).catch(err => { return reject(err) })
        });
    },
    getClients: () => {
        return new Promise((resolve, reject) => {
            mockyRepository.getClients().then(result => {
                return resolve(result);
            }).catch(err => { return reject(err) })
        });
    },
    getClientsByName: name => {
        return new Promise((resolve, reject) => {
            mockyRepository.getClients().then(result => {
                let clients = _.filter(result.data.clients, (client) => {
                    return _.includes(client.name, name)
                });
                return resolve(clients);
            }).catch(err => { return reject(err) })
        });
    },

}

