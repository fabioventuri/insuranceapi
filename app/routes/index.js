const middleWare = require('../config/middleware')

module.exports = app => {

  app.get('/', (req, res) => {
    res.status(200).send("Successfuly logout")
  })

  app.get('/auth/clients/:id', (req, res) => {
    let clientId = req.params.id;
    middleWare.auth(clientId).then((result) => {
      if (!result.data) return res.status(400).send(result)
      session = req.session;
      session.user = result.data;
      res.session(session).send(`Welcome ${session.user.role} ${session.user.name} `);
    }).catch((err) => {
      res.send(err)
    });
  })

  app.get('/logout', (req, res) => {
    req.session.destroy();
    res.send("Successfuly logout")
  })

  require('./clients')(app)
  require('./policies')(app)

}

