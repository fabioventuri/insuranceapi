const policiesController = require('../controller/policies')
const middleWare = require('../config/middleware')

module.exports = app => {
    app.get('/v1/clients/name/:name/policies', middleWare.requireRole('admin'), policiesController.getPoliciesByClientName);
}
