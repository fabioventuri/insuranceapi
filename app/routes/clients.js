const clientsController = require('../controller/clients')
const middleWare = require('../config/middleware')

module.exports = app => {
    app.get('/v1/clients/', clientsController.getClients);
    app.get('/v1/clients/:id', clientsController.getClient);
    app.get('/v1/clients/name/:name', clientsController.getClientByName);
    app.get('/v1/clients/policies/:id', middleWare.requireRole('admin'), clientsController.getClientByPolicy);
}
