const _ = require('lodash')
const axios = require('axios')

const MOCKY_BASEURL = 'http://www.mocky.io/v2/'
const CLIENTS_PATH = '5808862710000087232b75ac'
const POLICIES_PATH = '580891a4100000e8242b75c5'

module.exports = {
    getClients: () => {
        return axios.get(`${MOCKY_BASEURL}${CLIENTS_PATH}`)
    },
    getPolicies: () => {
        return axios.get(`${MOCKY_BASEURL}${POLICIES_PATH}`)
    },
}