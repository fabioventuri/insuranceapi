const request = require('supertest');
const assert = require('chai').assert
const app = require('../app');
const agent = request.agent(app);
const mockClient = {
    "id": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb",
    "name": "Manning",
    "email": "manningblankenship@quotezart.com",
    "role": "admin"
}

describe('GET /policies', function () {


    it('Test get policy by clientName Not authenticated', function (done) {
        agent.get(`/v1/clients/name/Britney/policies`)
            .set('Accept', 'application/json')
            .expect(403).end(function (err, res) {
                if (err) return done(err);
                done();
            });;

    });

    it('Test get policy by clientName authenticated', function (done) {
        agent.get(`/auth/clients/${mockClient.id}`)
            .set('Accept', 'application/json')
            .expect(200).end(function (err, res) {
                if (err) return done(err);

                agent.get(`/v1/clients/name/Britney/policies`)
                    .set('Accept', 'application/json')
                    .expect(200).end(function (err, res) {
                        if (err) return done(err);
                        // console.log(res.body)
                        assert.isNotNull(res.body)
                        assert.isTrue(res.body.length > 0)

                        done();
                    });;

            });;
    });

});
