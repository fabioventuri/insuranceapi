const request = require('supertest');
const assert = require('chai').assert
const app = require('../app');
const agent = request.agent(app);

const mockClient = {
    "id": "258a9db4-f585-4cde-9b28-f2d5cb2dd928",
    "name": "Margo",
    "email": "margoblankenship@quotezart.com",
    "role": "user"
}

const mockPolicy = {
    "id": "64cceef9-3a01-49ae-a23b-3761b604800b",
    "amountInsured": 1825.89,
    "email": "inesblankenship@quotezart.com",
    "inceptionDate": "2016-06-01T03:33:32Z",
    "installmentPayment": true,
    "clientId": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb"
}


describe('GET /', function () {
    it('Test server status', function (done) {
        request(app)
            .get('/')
            .set('Accept', 'application/json')
            .expect(200, done);
    });
});

describe('GET /clients', function () {
    it('Test get client by clientId', function (done) {
        request(app)
            .get(`/v1/clients/${mockClient.id}`)
            .set('Accept', 'application/json')
            .expect(200).end(function (err, res) {
                if (err) return done(err);
                let client = res.body;
                assert.equal(client.id, mockClient.id)
                assert.equal(client.name, mockClient.name)
                done();
            });;
    });

    it('Test get client by clientId not found', function (done) {
        request(app)
            .get(`/v1/clients/258a9db4-f585-4cde`)
            .set('Accept', 'application/json')
            .expect(404, done)
    });

    it('Test get client by name', function (done) {
        request(app)
            .get(`/v1/clients/name/${mockClient.name}`)
            .set('Accept', 'application/json')
            .expect(200).end(function (err, res) {
                if (err) return done(err);
                let client = res.body[0];
                assert.equal(client.id, mockClient.id)
                assert.equal(client.name, mockClient.name)
                done();
            });;
    });

});