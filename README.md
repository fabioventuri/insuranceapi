
## Installation

```bash
$ git clone https://fabioventuri@bitbucket.org/fabioventuri/insuranceapi.git
```

```bash
$ npm install
```

## Start the server:

```bash
$ npm start

```

## Test
```bash
$ npm test

```

## Use Docker
You can also run this app as a Docker container:

Step 1: Clone the repo

```bash
git clone https://fabioventuri@bitbucket.org/fabioventuri/insuranceapi.git
```

Step 2: Build the Docker image

```bash
docker build -t insuranceapi .
```

Step 3: Run the Docker container locally:

```bash
docker run -p 3000:3000 -d insuranceapi
```
